import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const firebase = require("firebase");
require("firebase/firestore");
firebase.initializeApp({
    apiKey: "AIzaSyBL1hcwogCpNEbiabSuiDw-KO2RrcCWTDA",
    authDomain: "evernote-clone-e2408.firebaseapp.com",
    databaseURL: "https://evernote-clone-e2408.firebaseio.com",
    projectId: "evernote-clone-e2408",
    storageBucket: "evernote-clone-e2408.appspot.com",
    messagingSenderId: "294548597145",
    appId: "1:294548597145:web:ab50393ceef8fbba"
});

ReactDOM.render(<App />, document.getElementById("root"));
