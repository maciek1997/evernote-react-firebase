import React from "react";
import { removeHTMLTags } from "../helpers";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import DeleteIcon from "@material-ui/icons/Delete";
import { ListItem, ListItemText } from "@material-ui/core";

export const SideBarItemComponent = props => {
    const selectNote = (n, i) => props.selectNote(n, i);
    const deleteNote = note => {
        if (window.confirm(`Are you sure you want to delete ${note.title}?`)) {
            props.deleteNote(note);
        }
    };
    const { _index, _note, classes, selectedNoteIndex } = props;
    return (
        <div key={_index}>
            <ListItem className={classes.listItem} selected={selectedNoteIndex === _index} alignItems={"flex-start"}>
                <div className={classes.textSection} onClick={() => selectNote(_note, _index)}>
                    <ListItemText
                        primary={_note.title}
                        secondary={removeHTMLTags(_note.body.substring(0, 30)) + "..."}
                    />
                </div>
                <DeleteIcon onClick={() => deleteNote(_note)} className={classes.deleteIcon} />
            </ListItem>
        </div>
    );
};

export default withStyles(styles)(SideBarItemComponent);
